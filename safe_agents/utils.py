import numpy as np
import matplotlib
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import itertools
from pathlib import Path
import math


def reject_outliers(data, m=2):
    mean = np.mean(data)
    std = np.std(data)
    distance = abs(data - mean)
    not_outlier = distance < m * std
    return data[not_outlier]

def save_frames_as_gif(agent, frames, path="./"):
    plt.figure(figsize=(frames[0].shape[1] / 72.0, frames[0].shape[0] / 72.0), dpi=72)
    patch = plt.imshow(frames[0])
    plt.axis("off")

    def animate(i):
        patch.set_data(frames[i])

    anim = animation.FuncAnimation(plt.gcf(), animate, frames=len(frames), interval=50)
    anim.save(path + agent.__class__.__name__ + ".gif", writer="imagemagick", fps=60)


def plot_visuals(agent, scores, safety, loc="./results/"):
    name = agent.__class__.__name__ + "#" + str(len(scores))
    Path(loc).mkdir(parents=True, exist_ok=True)
    sns.set_theme(style="darkgrid")

    fig, axs = plt.subplots(ncols=2, figsize=(15, 5))
    scores = reject_outliers(np.array(scores))
    scoredf = pd.DataFrame(
        [(i, v) for i, v in enumerate(scores)], columns=["episode", "score"]
    )
    sns.regplot(
        x="episode",
        y="score",
        data=scoredf,
        robust=True,
        ci=None,
        scatter_kws={"s": 10},
        ax=axs[0],
    )
    sns.boxplot(scores, showfliers=False, ax=axs[1])
    fig.savefig(loc + name + "-Scores.png", dpi=400)

    fig, ax = plt.subplots(ncols=1)
    risk_rates = []
    for j in safety:
        safe = j.count(1)
        unsafe = j.count(0)
        r = 0 if unsafe == 0 else (unsafe / (safe + unsafe))
        risk_rates.append(r)
    risk_rate_df = pd.DataFrame(
        [(i, v) for i, v in enumerate(risk_rates)], columns=["episode", "risk_rate"]
    )
    sns.lmplot(x="episode", y="risk_rate", data=risk_rate_df)
    plt.savefig(loc + name + "-Safety.png", dpi=400)


def plot_comparisson(data, episodes, loc="./results/"):
    for d in data:
        ep = [j for j in range(len(d["scores"]))]
        d.update({"episode": ep})
    df = pd.concat([pd.DataFrame(d) for d in data])
    r = []
    for i, row in df.iterrows():
        u = row["unsafe"]
        s = row["safe"]
        r.append(0 if u < 1 else u / (u + s))
    df["risk_rate"] = r

    sns.lmplot(
        x="episode",
        y="scores",
        data=df,
        hue="agent",
        scatter_kws={"s": 5},
        height=7,
        fit_reg=True,
        #robust=True,
        lowess=True,
        ci=90,
        truncate=True,
        sharex=True,
        #n_boot=math.ceil(episodes * 0.1),
    )
    plt.ylim([-900, 600])

    plt.savefig(loc + "".join([i["agent"] for i in data]) + "-Scores.png", dpi=400)
    sns.lmplot(
        x="episode",
        y="risk_rate",
        data=df,
        hue="agent",
        scatter_kws={"s": 5},
        height=7,
        fit_reg=True,
        robust=True,
        n_boot=math.ceil(episodes * 0.1),
    )
    plt.savefig(loc + "".join([i["agent"] for i in data]) + "-Safety.png", dpi=400)
