# fmt: off
# This gets rid of NumPy FutureWarnings that occur at TF import
import warnings
warnings.filterwarnings("ignore", category=FutureWarning)
# This gets rid of TF 2.0 related deprecation warnings
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import os
import sys
stderr = sys.stderr
sys.stderr = open(os.devnull, "w")
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam, RMSprop
from keras import backend as K
from keras import initializers
sys.stderr = stderr


def dqn_model(state_size, action_size, learning_rate=0.0001):
    model = Sequential()
    model.add(Dense(512, input_dim=state_size, activation="relu", kernel_initializer="he_uniform"))
    model.add(Dense(256, activation="relu", kernel_initializer="he_uniform"))
    model.add(Dense(action_size, activation="linear", kernel_initializer="he_uniform"))
    model.compile(loss="mse", optimizer=Adam(lr=learning_rate))
    return model


def actor_model(state_size, action_size, learning_rate=0.001):
    actor = Sequential()
    actor.add(Dense(128, input_dim=state_size, activation="relu"))
    actor.add(Dense(64, input_dim=state_size, activation="relu"))
    actor.add(Dense(32, input_dim=state_size, activation="relu"))
    actor.add(Dense(action_size, activation="softmax"))
    actor.compile(loss="categorical_crossentropy", optimizer=Adam(lr=learning_rate))
    return actor


def critic_model(state_size, action_size, learning_rate=0.005):
    critic = Sequential()
    critic.add(Dense(128, input_dim=state_size, activation="relu"))
    critic.add(Dense(64, input_dim=state_size, activation="relu"))
    critic.add(Dense(1, activation="linear"))
    critic.compile(loss="mse", optimizer=Adam(lr=learning_rate))
    return critic

# fmt: on
