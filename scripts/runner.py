import gym
import safe_agents as sa
import typer
import inspect
from pathlib import Path
from typing import List
import numpy as np

app = typer.Typer()
available = [i[0] for i in inspect.getmembers(sa.agents, inspect.isclass)]


def setup(a, env, save_loc):
    if a == "BaselineAgent":
        return sa.agents.BaselineAgent(env)
    elif a == "A2CControlAgent":
        return sa.agents.A2CControlAgent(env, save_loc)
    elif a == "A2CSafeAgent":
        return sa.agents.A2CSafeAgent(env, save_loc)
    elif a == "A2CAgent":
        return sa.agents.A2CAgent(env, save_loc)
    elif a == "DQNAgent":
        return sa.agents.DQNAgent(env, save_loc)
    elif a == "DQNControlAgent":
        return sa.agents.DQNControlAgent(env, save_loc)
    elif a == "DQNSafeAgent":
        return sa.agents.DQNSafeAgent(env, save_loc)


@app.command()
def train(
    agent: List[str] = typer.Option(..., help=f"Any of {str(available)}"),
    episodes: int = typer.Option(..., help="Number of training episodes"),
    env_name: str = typer.Option("LunarSafe-v0", help="Environment to run."),
    save_loc: str = typer.Option("./models/", help="Path to save to after training"),
    plot_loc: str = typer.Option("./results/", help="Path to save plots and GIF to"),
    compare: bool = typer.Option(False, help="Generate comparission plots"),
    render: bool = typer.Option(False, help="Show environment"),
):
    if any(a not in available for a in agent):
        typer.echo(f"[info] Unsupported agent. Available {str(available)}")
        raise typer.Abort()

    typer.echo(f"[info] Training {str(agent)} on environment {env_name}")
    env = gym.make(env_name)
    Path(save_loc).mkdir(parents=True, exist_ok=True)
    agents = [setup(a, env, save_loc) for a in agent]
    if compare:
        typer.echo(f"[info] Also setting up BaselineAgent for comparission")
        agents.append(setup("BaselineAgent", env, save_loc))
    data = []
    for a in agents:
        typer.echo(f"[info] Agent {a} started")
        scores, safety, frames = a.train(episodes=episodes, render=render)
        data.append(
            {
                "agent": str(a),
                "safe": [ep.count(1) for ep in safety],
                "unsafe": [ep.count(0) for ep in safety],
                "scores": scores,
            }
        )
        if render and frames is not None:
            typer.echo(f"[info] GIF of best run saved to {save_loc}")
            sa.utils.save_frames_as_gif(a, frames, plot_loc)
        typer.echo(f"[info] Weights of agent {a} saved to {save_loc}")
        sa.utils.plot_visuals(a, scores, safety, loc=plot_loc)
        typer.echo(f"[info] Agent {a} plots saved to {plot_loc}")
    if compare:
        typer.echo(
            f"[info] Comparing performance of {str(agent)} against BaselineAgent"
        )
        sa.utils.plot_comparisson(data, episodes, loc=plot_loc)
        typer.echo(f"[info] Comparission saved to {plot_loc}")
    typer.echo(f"[info] Finished. Goodbye.")


@app.command()
def evaluate(
    agent: List[str] = typer.Option(..., help=f"Any of {str(available)}"),
    episodes: int = typer.Option(..., help="Number of training episodes"),
    env_name: str = typer.Option("LunarSafe-v0", help="Environment to run."),
    save_loc: str = typer.Option("./models/", help="Path to save to after training"),
    plot_loc: str = typer.Option("./results/", help="Path to save plots and GIF to"),
    render: bool = typer.Option(False, help="Show environment"),
):
    if any(a not in available for a in agent):
        typer.echo(f"[info] Unsupported agent. Available {str(available)}")
        raise typer.Abort()

    typer.echo(f"[info] Evaluating {str(agent)} on environment {env_name}")
    env = gym.make(env_name)
    agents = [setup(a, env, save_loc) for a in agent]
    agents = [a.load() for a in agents]
    state_size = env.observation_space.shape[0]
    action_size = env.action_space.n
    for a in agents:
        typer.echo(f"[info] Agent {a} started")
        total_reward, safety, bframes = [], [], []
        for e in range(episodes):
            done = False
            state = env.reset()
            scores = 0
            status = []
            frames = []
            while not done:
                if render:
                    frames.append(env.render(mode="rgb_array"))
                action = a.get_action(state)
                state, reward, done, info = env.step(action)
                status.append(info["status"])
                scores += reward
                if done:
                    total_reward.append(scores)
                    safety.append(status)
                    safe = status.count(1)
                    unsafe = status.count(0)
                    risk_rate = 0 if unsafe == 0 else unsafe / (safe + unsafe)
                    print(
                        f"\tepisode: {e}  | "
                        f"score: {scores}  | "
                        f"risk_rate {risk_rate}"
                    )
                    if scores == np.max(total_reward) and (e > episodes * 0.1):
                        bframes = frames
                    if np.average(total_reward[-100:]) > 200 and (e > episodes * 0.1):
                        print(f"\t>>> Solved environment! Stopping early.")
                        return
        if render:
            typer.echo(f"[info] Best run saved to {save_loc}")
            sa.utils.save_frames_as_gif(a, frames, plot_loc)
        print("\t======================")
        print(f"\ttotal_reward: {sum(total_reward)}")
        print(
            f"\ttotal_risk_rate {sum(x.count(0) for x in safety)/sum(x.count(1) for x in safety)}"
        )
        print("\t======================")
    typer.echo(f"[info] Finished. Goodbye.")


if __name__ == "__main__":
    app()
